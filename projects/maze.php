

        <!doctype html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/snippets/head.php'?>
</head>

<body>
    <div id="background">
        <div id="thebox">
            <header>
                <?php include $dir_navigation?>
            </header>

            <div id="content" class="container">
                    <div class="container blogpost">
                    <h4>
            3D Maze Game </h4>
        <p class="experience-period">
            Aug 2014 - Sep 2014 </p>
        <p>
            A 3D maze of cubes is procedually generated. It can be explored by walking.
            <br />
            I conducted this project in my free time. Most of the game code I wrote from scratch.
        </p>
    <img src="/assets/images/project/maze.webp" width="100%" height="auto" />
                </div>

            <footer>
                <?php include $dir_contact?>
            </footer>
        </div>
    </div>
</body>

</html>

