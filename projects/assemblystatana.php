<!doctype html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/snippets/head.php'?>
</head>

<body>
    <div id="background">
        <div id="thebox">
            <header>
                <?php include $dir_navigation?>
            </header>

            <div id="content" class="container">
                    <div class="container blogpost">
                        <h1>Static Analysis of Assembly Code for Bufferoverflow Vulnerabilities in Python</h1>

                        <p class="post-metadata">
                            <span class="metadata">
                                <i class="fa fa-calendar"></i>
                                Oct - Dec 2018</span>
                        </p>
                        
                        Find the
                        <a href="https://gitlab.com/Schippmunk/AssemblyStatAna">Project on GitLab</a>
                        
                        In a group of three we wrote a python script that takes as input simplified assembly code and produces a list of bufferoverflow vulnerabilites.
                        
                    </div>
                </div>

            <footer>
                <?php include $dir_contact?>
            </footer>
        </div>
    </div>
</body>

</html>