<!doctype html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/snippets/head.php'?>
</head>

<body>
    <div id="background">
        <div id="thebox">
            <header>
                <?php include $dir_navigation?>
            </header>

            <div id="content" class="container">
                <div class="container blogpost">

                    <h4>
                        The Jaynes-Cummings Model in MATLAB </h4>
                    <p class="experience-period">
                        Feb - Mar 2017 </p>
                    <p>
                        The Wigner transform of a discretized electromagnetic field was investigated with respect to
                        various parameters.
                    </p>
                    <img src="/assets/images/project/jaynescummings.webp" width="100%" height="auto" />
                </div>
            </div>

            <footer>
                <?php include $dir_contact?>
            </footer>
        </div>
    </div>
</body>

</html>