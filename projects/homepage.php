<!doctype html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/snippets/head.php'?>
</head>

<body>
    <div id="background">
        <div id="thebox">
            <header>
                <?php include $dir_navigation?>
            </header>

            <div id="content" class="container">
                <div class="container blogpost">
                    <h4>
                        This Homepage.
                    </h4>
                    Find the <a href="https://gitlab.com/Schippmunk/schippmunk-hp">Source Code on GitLab</a>. I started
                    creating this page in April 2019.

                    <p>
                        I use SASS rather than CSS to style the website. This inspired me to write some simple bash
                        scripts to help me build the site. During development
                        I have the built-in PHP web server running. Additionally upon server startup, another process
                        spawns. This process uses find and grep to scan the
                        directory for relevant files. These are piped into entr. This way, whenever a file is written
                        to, a couple of things happen:
                        <ul>
                            <li>Scss files get compiled to sass.</li>
                            <li>Images get compressed.</li>
                            <li>The current browser tab reloads.</li>
                        </ul>
                        The last bullet point happens using <a href="http://eradman.com/entrproject/scripts/">this sweet
                            script</a> by Eric Radman.
                    </p>

                </div>

                <footer>
                    <?php include $dir_contact?>
                </footer>
            </div>
        </div>
</body>

</html>