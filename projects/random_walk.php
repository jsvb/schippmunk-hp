<!doctype html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/snippets/head.php'?>
</head>

<body>
    <div id="background">
        <div id="thebox">
            <header>
                <?php include $dir_navigation?>
            </header>

            <div id="content" class="container">
                <div class="container blogpost">
                    <h4>
                        2D Random Walk simulation </h4>
                    <p class="experience-period">
                        Oct - Nov 2016 </p>
                    <p>
                        Using Matlab, numerical approximations are given for the Narrow Escape Problem in the 2D
                        unit sphere.
                    </p>
                    <img src="/assets/images/project/randomwalk.webp" width="100%" height="auto" />
                </div>
            </div>

            <footer>
                <?php include $dir_contact?>
            </footer>
        </div>
    </div>
</body>

</html>