

        <!doctype html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/snippets/head.php'?>
</head>

<body>
    <div id="background">
        <div id="thebox">
            <header>
                <?php include $dir_navigation?>
            </header>

            <div id="content" class="container">
                    <div class="container blogpost">
                    <h4>
            Käsekästchen CLI Game </h4>
        <p class="experience-period">
            Nov 2015 - Mar 2016 </p>
        <p>
            A Käsekästchen, also known as <a href="https://en.wikipedia.org/wiki/Dots_and_Boxes">Dots
                and Boxes</a> type game played in the console. With special effects and an AI enemy.
        </p>
    <img src="/assets/images/project/kaesekaestchen.webp" width="100%" height="auto" />
                </div>

            <footer>
                <?php include $dir_contact?>
            </footer>
        </div>
    </div>
</body>

</html>

