<!doctype html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/snippets/head.php'?>
</head>

<body>
    <div id="background">
        <div id="thebox">
            <header>
                <?php include $dir_navigation?>
            </header>

            <div id="content" class="container">
                <div class="container blogpost">
                    <h4>
                        Gorillas Game </h4>
                    <p class="experience-period">
                        Feb 2015 - Mar 2015 </p>
                    <p>
                        Two players control one gorilla each. Procedually, a level of colorful tall buildings is
                        generated. Each round an angle and an intensity is entered and a banana thrown accordingly.
                        The banana explodes on impact. Also contains car and rain animations.
                        <br />
                        We used the Java game library Slick2D and heavy JUnit testing.
                    </p>
                    <img src="/assets/images/project/gorillas.webp" width="100%" height="auto" />
                </div>

                <footer>
                    <?php include $dir_contact?>
                </footer>
            </div>
        </div>
</body>

</html>