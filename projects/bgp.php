<!doctype html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/snippets/head.php'?>
</head>

<body>
    <div id="background">
        <div id="thebox">
            <header>
                <?php include $dir_navigation?>
            </header>

            <div id="content" class="container">
                <div class="container blogpost">
                    <h1>Boarder Gateway Protocol in Promela</h1>

                    <p class="post-metadata">
                        <span class="metadata">
                            <i class="fa fa-calendar"></i>
                            2018-12-23</span>
                    </p>

                    <p>Border Gateway Protocol (<a href="https://en.wikipedia.org/wiki/Border_Gateway_Protocol">BGP</a>)
                        is
                        a standardized exterior gateway protocol designed to exchange routing and
                        reachability
                        information among autonomous systems on the Internet. The protocol is classified as
                        a path vector
                        protocol.
                        The Border Gateway Protocol makes routing decisions based on paths, network
                        policies, or rule-sets
                        configured by a network administrator
                        and is involved in making core routing decisions.</p>
                    <p>Here, an abstraction of the BGP is considered. Given a graph, there is a
                        distinguished node t. The
                        aim of BGP is that
                        each node finds a path to communicate with t. An edge between two nodes indicates
                        that these nodes
                        can communicate. Each node different from t
                        has a contract table, and lets its predecessors know how much it will cost them to
                        use this node to
                        communicate with t. The nodes always select the minimum cost.</p>
                    <p>In promela, each node is modeled by a process (or proctype). It is required for the
                        nodes to keep
                        track of the costs and paths to t, the successors offer.
                        Whenever a node receives an updated path, it needs to be checked for loops, and
                        propagated to the
                        predecessors of the receiving node.</p>
                    <p>In Linear Temporal Logic (<a href="https://en.wikipedia.org/wiki/Linear_temporal_logic">LTL</a>)
                        the
                        property that BGP converges is specified and can be validated in <a
                            href="http://spinroot.com/spin/whatispin.html">SPIN</a>. Convergence means that
                        at some point,
                        all nodes will have found a fixed path.</p>
                    <p>Here, we see an example of a simulation of a non-convergent BGP graph in SpinRCP. The
                        two nodes n1
                        and n2 keep sending the same value back and forth.</p>
                    <p><img src="/assets/images/project/simulation.webp" alt="Simulation" /></p>
                    <p>Sadly, SPIN is not used for model checking in practice, because it is too slow. For
                        this relatively
                        simple algorithms it already does not make sense
                        to run a simulation on a graph of more than 10 nodes. In industry, the tool of
                        choice is <a href="http://nusmv.fbk.eu/">NuSMV</a></p>
                    <p>Find the source code on <a href="https://github.com/Schippmunk/BGPonGraph/"
                            title="Github">GitHub</a>. Feel free to play around with it!</p>

                </div>
            </div>

            <footer>
                <?php include $dir_contact?>
            </footer>
        </div>
    </div>
</body>

</html>