<!doctype html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/snippets/head.php'?>
</head>

<body>
    <div id="background">
        <div id="thebox">
            <header>
                <?php include $dir_navigation?>
            </header>

            <div id="content" class="container">
                <div class="container blogpost">
                    <h4>
                        Zombie Game </h4>
                    <p class="experience-period">
                        Jul 2014 </p>
                    <p>
                        The player tries to defeat the zombie in a land full of dangers.
                        <br />
                        This is a tile based 2D Java game I developed in my free time.
                    </p>
                    <img src="/assets/images/project/zombie.webp" width="100%" height="auto" />
                </div>

                <footer>
                    <?php include $dir_contact?>
                </footer>
            </div>
        </div>
</body>

</html>