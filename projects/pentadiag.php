<!doctype html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/snippets/head.php'?>
</head>

<body>
    <div id="background">
        <div id="thebox">
            <header>
                <?php include $dir_navigation?>
            </header>

            <div id="content" class="container">
                <div class="container blogpost">
                    <h4>
                        Extension of a tridiagonal to a pentadiagonal solver </h4>
                    <p class="experience-period">
                        Oct - Nov 2016 </p>
                    <p>
                        Solving general banded systems in serial requires solving a large sparse matrix, which most
                        often can be a slow process even with the most efficient algorithm. Obtaining a solution to
                        a pentadiagonal system requires four homogeneous solutions, each with an undetermined
                        coefficient added to a particular solution. These coefficients depend on coupling to
                        neighbouring solutions. We developed said algorithm. The original matrix is decomposed into
                        contiguous subsystems which are passed to each processor. The global solution is then
                        obtained as a linear combination of the local solutions.
                    </p>
                </div>

                <footer>
                    <?php include $dir_contact?>
                </footer>
            </div>
        </div>
</body>

</html>