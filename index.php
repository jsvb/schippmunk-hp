<!doctype html>
<html lang="en">

<head>
    <?php include __DIR__ . '/snippets/head.php'?>
</head>

<body>
    <div id="background">
        <div id="thebox">
            <header>
                <?php include $dir_navigation?>
            </header>

            <div id="content">
                <?php include $dir_con_portfolio ?>
            </div>

            <footer>
                <?php include $dir_contact?>
            </footer>
        </div>
    </div>
</body>

</html>