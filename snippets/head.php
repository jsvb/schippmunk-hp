<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="description" content="Homepage of JSVB">
<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
<!--<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />-->
<meta name="author" content="Johannes Schipp von Branitz">

<!-- Fontawesome-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
    integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<!-- my css -->
<link rel="stylesheet" href="/assets/css/app.css">
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<title>jsvb</title>

<!-- JavaScript -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>

<?php
    $dir_root = $_SERVER['DOCUMENT_ROOT'];
    $dir_navigation = $dir_root . '/snippets/navigation.php'; 
    $dir_contact = $dir_root . '/snippets/contact.php';
    $dir_con_portfolio = $dir_root .'/snippets/content_portfolio.php';
    $dir_con_blog = $dir_root .'/snippets/content_blog.php';
    $dir_con_gallery = $dir_root . '/snippets/content_gallery.php';
    $dir_gallery_images = $dir_root . '/assets/images/gallery';
?>
