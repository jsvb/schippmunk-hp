<ul id="blog-list">
    <li>
        <article>
            <span class="post-title">
                <a href="/blog/bgp.php">
                    Boarder Gateway Protocol in Promela
                </a>
            </span>
            <span class="post-meta text-muted">
                    <i class="fas fa-calendar"></i> 2018-12-23
            </span>
        </article>
    </li>
</ul>
