<div class="contact">
    <ul>
        <li>
            </header>
            <a href="https://math.stackexchange.com/users/355263/jsvb">
                <i class="fab fa-stack-exchange"></i></a>
        </li>
        <li>
            <a href="https://gitlab.com/Schippmunk">
                <i class="fab fa-gitlab"></i></a>
        </li>  
        <li>
            <a href="https://github.com/Schippmunk">
                <i class="fab fa-github"></i></a>
        </li>
        <li>
            <a href="mailto:schippmunk@gmx.de">
                <i class="fas fa-envelope"></i>
            </a>
        </li>
    </ul>
</div>
