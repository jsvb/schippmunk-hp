<div id="portfolio">

<h1>Johannes Schipp von Branitz</h1>
<hr/>

<div class="portfoliosection floatcenter">
<div>
        &ldquo;If people do not believe that mathematics is simple, it is only because they do not realize
        how complicated life is.&ldquo;<br />- John von Neumann 1947
</div> 
</div>
<hr />

<div class="portfoliosection floatleft">
    <h2>
        Research
    </h2>
    <div>
    Master's Thesis: 
      <a href="https://github.com/Schippmunk/cubical">Higher Groups via Displayed Univalent Reflexive Graphs in Cubical Type Theory </a>
</ br>
    Bachelor's Thesis: Structure Results and Generators for Congruence Subgroups and Application to the Weil
    Representation.
    </div>
</div>

<div class="portfoliosection floatright">
    <h2>
        Teaching and Conferences
    </h2>

    <div>
      <a href="https://wiki.portal.chalmers.se/agda/Main/AIMXXXIII">Talk "Displayed Univalent Reflexive Graphs in CTT" at the AIMXXXIII (22.10.2020)</a>
    </div>

    <div>
        <a href="https://www.mathematik.tu-darmstadt.de/algebra/arbeitsgruppe_algebra/forschung_algebra/konferenzen_und_workshops_ag_algebra/modular_forms_on_higher_rank_groups/mod_forms_on_higher_rank_groups.en.jsp">
            Conference on Modular Forms on Higher Rank Groups at TU Darmstadt (2019)
        </a>
    </div>

    <div>
        Tutor for Multivariate Analysis at TU Darmstadt (2017)
    </div>
    
</div>


<div class="portfoliosection floatleft">
    <h2>
        Mathematics Seminars
    </h2>

    <div>
        Automorphic Forms: Classification of Even Unimodular Lattices in Dimension 24 (Apr - Jul 2019)
    </div>

    <div>
        Modal Logics: Computation Tree Logic and Monadic Path Logic (Oct - Dec 2018)
    </div>

    <div>
        Navier-Stokes equations: Weak Formulation (Apr - Jul 2017)
    </div>

</div>


<div class="portfoliosection floatright">
    <h2>
        Programming Projects
    </h2>

    <div>
        <a href="/projects/homepage.php">
            This Homepage (May 2019)
        </a>
    </div>

    <div>
        <a href="/projects/assemblystatana.php">
            Static Analysis of Assembly Code for Bufferoverflow Vulnerabilities in Python (Oct - Dec 2018)
        </a>
    </div>

    <div class="">

        <a href="/projects/bgp.php">
            Boarder Gateway Protocol in PROMELA and Python (Nov - Dec 2018)
        </a>
    </div>

    <div class="">
        <a href="/projects/jaynes_cummings.php">
            Simulation of the Jaynes-Cummings Model in MATLAB (Feb - Mar 2017)
        </a>
    </div>

    <div class="">
        <a href="/projects/pentadiag.php">
            Extension of a tridiagonal to a pentadiagonal solver (Oct - Nov 2016)
        </a>

    </div>
    <div class="">
        <a href="/projects/random_walk.php">
            2D Random Walk simulation in MATLAB (Oct - Nov 2016)
        </a>
    </div>

    <div class="">
        <a href="/projects/kaesekaestchen.php">
            Käsekästchen CLI Game in Java (Nov 2015 - Mar 2016)
        </a>
    </div>

    <div class="">
        <a href="/projects/gorillas.php">
            Gorillas Game in Java (Feb 2015 - Mar 2015)
        </a>
    </div>

    <div class="">
        <a href="/projects/maze.php">
            3D Maze Game in Java (Aug 2014 - Sep 2014)
        </a>
    </div>

    <div class="">
        <a href="/projects/zombie.php">
            Zombie Game in Java (Jul 2014)
        </a>
    </div>
</div>


</div>
