#!/bin/bash

echo "Welcome to the script!"

rootdir="/home/jsvb/Git/schippmunk-hp"
echo "Changing directory to "$rootdir
cd $rootdir

# start the server in deploy directory and output php server log to logs/phpserver.log
echo "Starting the server at "$rootdir"/deploy and logging to "$rootdir"/logs/phpserver.log"
php -S localhost:8000 -t $rootdir"/deploy" &>> $rootdir"/logs/phpserver.log" &
spid=$!
echo "Server PID is "$spid

run=true

while [ "$run" = true ]
do
    read -p "Expecting input: (deploy (d)|pushtoprod|pushtogitlab|convert|reload|stop)" input

    case $input in
        "d" )
            echo "Starting full deployment to ./deploy"

            cd $rootdir

            # read scss compile mode and pass it to sassc
            echo "Compiling scss to assets/css"

            sassc scss/app.scss -t compressed assets/css/app.css

            # create fresh deploy directory
            echo "Cleaning deploy directory"
            mkdir -p deploy
            cd deploy
            ls | xargs rm -r
            cd ..

            # copy the needed files
            echo "Copying files"
            cp favicon.ico deploy/favicon.ico
            cp index.php deploy/index.php
            cp portfolio.php deploy/portfolio.php
            cp gallery.php deploy/gallery.php

            # recursively copy the needed folders
            cp -r snippets deploy/snippets
            cp -r projects deploy/projects
            cp -r assets deploy/assets
            cp -r galleryfolders deploy/galleryfolders

            
            echo "Size in KB is "
            du -ks deploy
            ;;
        convert )
            echo "Convertig images to webp"
            cd $rootdir

            echo "Converting general folder"
            rm assets/images/general/*
            dest="assets/images/general/"
            du -k images/general
            for i in images/general/*; do
                filename=$(basename -s .png $i)
                filename=$(basename -s .jpg $filename)
                filename=$(basename -s .JPG $filename)
                filename=$(basename -s .jpeg $filename)
                cwebp -q 50 -o $dest$filename".webp" $i &>> logs/build.log
            done
            du -k assets/images/general

            echo "Converting project folder"
            rm assets/images/project/*
            dest="assets/images/project/"
            du -k images/project
            for i in images/project/*; do
                filename=$(basename -s .png $i)
                filename=$(basename -s .jpg $filename)
                filename=$(basename -s .JPG $filename)
                filename=$(basename -s .jpeg $filename)
                cwebp -q 50 -o $dest$filename".webp" $i &>> logs/build.log
            done
            du -k assets/images/project

            echo "Converting gallery folder"
            dest="assets/images/gallery/amrum/"
            for i in images/gallery/amrum/*; do
                filename=$(basename -s .png $i)
                filename=$(basename -s .jpg $filename)
                filename=$(basename -s .JPG $filename)
                filename=$(basename -s .jpeg $filename)
                cwebp -q 50 -o $dest$filename".webp" $i &>> logs/build.log
            done
            
            ;;
        pushtoprod )
            echo "Pushing deploy directory to prod remote repository."

            cd $rootdir"/deploy"

            git status
            git add .
            git commit
            git push prod master
            ;;
        pushtogitlab )
            echo "Pushing project to GitLab."

            cd $rootdir

            git status
            git add .
            git commit
            git push
            ;;
        reload )
            cd $rootdir
            find | grep '.*.php' | entr -p ./scripts/reload-browser.sh Firefox &>> ./logs/build.log &
            rbpid=$!
            echo "Reload Browser PID is "$rbpid
            ;;
        stop )
            run=false
            break
            ;;
        *)
            ;;
    esac

done

# cleanup: kill processes
echo "Killing Processes"
kill $spid

if [ -n "$rbpid" ]
then
    kill $rbpid
fi
            

echo "End of all.sh"
