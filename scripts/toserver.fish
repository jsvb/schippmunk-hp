#!/bin/fish


# start an ssh-agent so that the password does not have to be reentered all the time
# in bash this is eval $(ssh-agent)
echo "Starting SSH Agent"
eval (ssh-agent -c)
ssh-add

echo "Reading the first file"
# read a file name and content
read -P "Filename: " file
read -P "Content: " content

# update file path
set file ".well-known/acme-challenge/"$file

# define the command to be executed over ssh
set cmd "echo $content > $file"

# execute the command on the webserver
echo "Executing first command on the server"
ssh jsvb_schippmunk@ssh.phx.nearlyfreespeech.net $cmd

# do it all again
echo "Do it again"
read -P "Filename: " file
read -P "Content: " content
set file ".well-known/acme-challenge/"$file
set cmd "echo $content > $file"
ssh jsvb_schippmunk@ssh.phx.nearlyfreespeech.net $cmd

# copy certificates to clipboard
echo "Nice. Copy certificates to clipboard?"
read -P "OK. Do it"

set fullchain (sudo cat /etc/letsencrypt/live/www.jsvb.xyz/fullchain.pem)
set privkey (sudo cat /etc/letsencrypt/live/www.jsvb.xyz/privkey.pem)
set combined "$fullchain"\n"$privkey"
echo $combined | xclip -selection c -i 


