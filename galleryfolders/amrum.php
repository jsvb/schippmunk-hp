<!doctype html>
<html lang="en">

<head>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/snippets/head.php'?>
</head>

<body>
    <div id="background">
        <div id="thebox">
            <header>
                <?php include $dir_navigation?>
            </header>

            <div id="content" class="container gallery">

            <ul id="images">
        <?php
            $subdir = '/amrum';
            $imagenames = array_diff(scandir($dir_gallery_images . $subdir), array('..', '.'));
            $dir_gallery_images_relative = "assets/images/gallery" . $subdir;
            foreach ($imagenames as $imagename){
                echo '<li>';
                    echo '<img src="/' .  $dir_gallery_images_relative . '/' .  $imagename . '" height="200px" alt="' . $imagename . '">'; 
                echo '</li>';
            }
        ?>
    </div>
        </ul>

                </div>

            <footer>
                <?php include $dir_contact?>
            </footer>
        </div>
    </div>
</body>

</html>